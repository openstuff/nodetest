module.exports = {
    'dispo-file': {
      output: {
        mode: 'single',
        target: './src/dispo.ts',
        schemas: './src/model',
        mock: true,
      },
      input: {
        target: './openApi.yaml'
      },
    },
  };